#!/bin/bash

source activate tensorflow_cse576 
export DATA_DIR='/home/rajat/Documents/MS/Spring 2018/CSE 576/final_project/cse576-final-project/datasets/KITTI/data'
echo $DATA_DIR


python kitti2tf.py \
	--data_dir="$DATA_DIR" \
	--output_path="$DATA_DIR/tf_record/kitti.record" \
	--label_map_path="$DATA_DIR/kitti_label_map.pbtxt" \
	--classes_to_use="car, pedestrian, cyclist, truck, dontcare" \
	--validation_set_size="1496"